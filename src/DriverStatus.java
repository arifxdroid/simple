import javafx.scene.paint.Color;

public class DriverStatus {

	private Color color;
	private float startPoint = 90.0f;
	private float endPoint;
	private String startTime;
	private String endTime;
	
	final private int DRIVING = 1;
	final private int NOTDRIVING = 2;
	final private int OFFDUTY = 3;
	final private int SLEEPING = 4;

	public DriverStatus(String startTime, int drivingState) {
		super();
		this.startTime = startTime;
		this.startPoint = calculatePointFromTime(startTime);
		setColor(drivingState);
	}

	public String getStartTime() {
		return startTime;
	}

	public Color getColor() {
		return color;
	}

	public float getStartPoint() {
		return startPoint;
	}

	public String getEndTime() {
		return endTime;
	}

	public float getEndPoint() {
		return endPoint;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
		setEndPoint(endTime);
	}

	private void setEndPoint(String endTime) {

		this.endPoint = calculatePointFromTime(endTime);
	}

	public void setEndPoint(float endPoint) {
		this.endPoint = endPoint;
	}

	public void setStartPoint(float startPoint) {
		this.startPoint = startPoint;
	}

	private void setColor(int drivingState) {

		switch (drivingState) {

		case DRIVING:
			this.color = ColorCustom.DRIVING_COLOR;
			break;
			
		case NOTDRIVING:
			this.color = ColorCustom.NOT_DRIVING_COLOR;
			break;

		case OFFDUTY:
			this.color = ColorCustom.OFF_DUTY_COLOR;
			break;


		case SLEEPING:
			this.color = ColorCustom.SLEEP_COLOR;
			break;

		default:
			this.color = ColorCustom.OFF_DUTY_COLOR;
			break;
		}

	}

	private float calculatePointFromTime(String timeInput) {
		String timeFormat = null;
		float time = 0;
		float point = 360;
		float timeDecimal;
		float minute;
		float hour;

		if (timeInput.toLowerCase().indexOf("am".toLowerCase()) != -1) {

			timeFormat = "am";
			time = Float.parseFloat(extractNumber(timeInput, timeFormat));

		} else if (timeInput.toLowerCase().indexOf("pm".toLowerCase()) != -1) {

			timeFormat = "pm";
			time = Float.parseFloat(extractNumber(timeInput, timeFormat));

		} else {
			System.out.println("not found");
		}

		switch (timeFormat) {
		case "am":
			if (time>=12.0 && time <= 13) {
				minute = (float) (time - Math.floor(time));
				hour = time - minute;
				timeDecimal = ((minute * 100) / 60);
				point = 90 - timeDecimal * 15;
			} else {
				minute = (float) (time - Math.floor(time));
				hour = time - minute;
				timeDecimal = hour + ((minute * 100) / 60);
				point = timeDecimal * 15;
			}
			
			break;
		case "pm":
			if (time>=12.0 && time <= 13) {
				minute = (float) (time - Math.floor(time));
				hour = time - minute;
				timeDecimal = ((minute * 100) / 60);
				point = 180 + timeDecimal * 15;
			} else {
				minute = (float) (time - Math.floor(time));
				hour = 12+ time - minute;
				timeDecimal = hour + ((minute * 100) / 60);
				point = timeDecimal * 15;
			}
			break;
		}

		return point;
	}

	private static String extractNumber(String str, String format) {

		return str.replace(format, "");
	}

}
