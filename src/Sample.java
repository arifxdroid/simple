import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Sample extends Application {

	int DRIVING = 1;
	int NOTDRIVING = 2;
	int OFFDUTY = 3;
	int SLEEPING = 4;

	ArrayList<DriverStatus> driverStatus = new ArrayList<>();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Drawing Operations Test");
		Group root = new Group();
		Canvas canvas = new Canvas(500, 500);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		drawShapes(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void insertValues() {
		driverStatus.add(new DriverStatus("5.30am", NOTDRIVING));
		driverStatus.add(new DriverStatus("7am", DRIVING));
		driverStatus.add(new DriverStatus("8am", NOTDRIVING));
		driverStatus.add(new DriverStatus("8.30am", DRIVING));
		driverStatus.add(new DriverStatus("9am", NOTDRIVING));
		driverStatus.add(new DriverStatus("10.30am", DRIVING));
		driverStatus.add(new DriverStatus("4.30pm", NOTDRIVING));
		driverStatus.add(new DriverStatus("5.30pm", DRIVING));
		driverStatus.add(new DriverStatus("8pm", OFFDUTY));
	}

	private void dynamicValueDisplay(GraphicsContext gc) {
		DriverStatus startingStatus = new DriverStatus("12am", SLEEPING);
		int i = 0;
		float startPoint = 90.0f;
		float endPoint = 0;
		boolean emptyArrayList = (driverStatus.size() == 0);
		boolean twoValueInArrayList = (driverStatus.size() >= 2);
		boolean lastElementOfArrayList;

		if (!emptyArrayList) {
			if (twoValueInArrayList) {
				startingStatus.setEndPoint(driverStatus.get(0).getStartPoint());
			} else {
				// driverStatus.get(0).setEndPoint(-90f);
			}
			gc.setFill(startingStatus.getColor());
			gc.fillArc(100, 100, 300, 300, startPoint = startingStatus.getStartPoint(),
					endPoint = -startingStatus.getEndPoint(), ArcType.ROUND);

			System.out.println("start point 1- " + startPoint);
			System.out.println("end point 1- " + endPoint);

			for (i = 0; i < driverStatus.size(); i++) {
				lastElementOfArrayList = (i == driverStatus.size() - 1);

				if (!lastElementOfArrayList) {
					driverStatus.get(i)
							.setEndPoint(driverStatus.get(i + 1).getStartPoint() - driverStatus.get(i).getStartPoint());
				} else {
					// driverStatus.get(i).setEndPoint(360f);

				}

				gc.setFill(driverStatus.get(i).getColor());
				gc.fillArc(100, 100, 300, 300, startPoint = startPoint + endPoint,
						endPoint = -driverStatus.get(i).getEndPoint(), ArcType.ROUND);

				System.out.println("start point- " + startPoint);
				System.out.println("end point- " + endPoint);
			}
		}

	}

	private void drawShapes(GraphicsContext gc) {

		int centerPointX = 250;
		int centerPointY = 250;
		Font quartzFont;

		insertValues();

		// Full Background color
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, 500, 500);

		// border draw black
		gc.strokeOval(100, 100, 300, 300);

		// default color, off duty
		gc.setFill(ColorCustom.OFF_DUTY_COLOR);
		gc.fillOval(100, 100, 300, 300);

		// -----------Dynamic value starts here------------------------
		
		dynamicValueDisplay(gc);

		// -----------Dynamic value ends here----------------------------

		// Center color white
		gc.setFill(Color.WHITE);
		gc.fillOval(125, 125, 300 - 50, 300 - 50);
		// Center border line
		gc.strokeOval(125, 125, 300 - 50, 300 - 50);

		// *************DRAW LINE TICK*******************
		// Line tick that i mean here is, short line to indicate second.

		for (int i = 1; i <= 360; i++) {
			int tickX;
			int tickY;

			int tickXb;
			int tickYb;

			tickX = (int) (Math.cos(i * Math.PI / 12 - Math.PI / 2) * 150 + centerPointX);
			tickY = (int) (Math.sin(i * Math.PI / 12 - Math.PI / 2) * 150 + centerPointY);

			tickXb = (int) (Math.cos(i * Math.PI / 12 - Math.PI / 2) * 156 + centerPointX);
			tickYb = (int) (Math.sin(i * Math.PI / 12 - Math.PI / 2) * 156 + centerPointY);
			gc.strokeLine(tickXb, tickYb, tickX, tickY);

			tickX = (int) (Math.cos(i * Math.PI / 24 - Math.PI / 2) * 150 + centerPointX);
			tickY = (int) (Math.sin(i * Math.PI / 24 - Math.PI / 2) * 150 + centerPointY);

			tickXb = (int) (Math.cos(i * Math.PI / 24 - Math.PI / 2) * 152 + centerPointX);
			tickYb = (int) (Math.sin(i * Math.PI / 24 - Math.PI / 2) * 152 + centerPointY);
			gc.strokeLine(tickXb, tickYb, tickX, tickY);

		}
		// *************DRAW LINE TICK*******************

		// Draw number 12 to 1
		quartzFont = new Font("Verdana", 10);
		gc.setFont(quartzFont);
		gc.strokeText("12am", centerPointX - 15, centerPointY - 160);
		gc.strokeText("1", centerPointX + 40, centerPointY - 155);
		gc.strokeText("2", centerPointX + 78, centerPointY - 139);
		gc.strokeText("3", centerPointX + 111, centerPointY - 113);
		gc.strokeText("4", centerPointX + 137, centerPointY - 79);
		gc.strokeText("5", centerPointX + 153, centerPointY - 40);

		gc.strokeText("6am", centerPointX + 160, centerPointY + 2);
		gc.strokeText("7", centerPointX + 153, centerPointY + 45);
		gc.strokeText("8", centerPointX + 138, centerPointY + 83);
		gc.strokeText("9", centerPointX + 112, centerPointY + 118);
		gc.strokeText("10", centerPointX + 73, centerPointY + 146);
		gc.strokeText("11", centerPointX + 36, centerPointY + 161);

		gc.strokeText("12pm", centerPointX - 15, centerPointY + 165);
		gc.strokeText("1", centerPointX - 47, centerPointY + 160);
		gc.strokeText("2", centerPointX - 85, centerPointY + 145);
		gc.strokeText("3", centerPointX - 118, centerPointY + 119);
		gc.strokeText("4", centerPointX - 144, centerPointY + 85);
		gc.strokeText("5", centerPointX - 160, centerPointY + 45);

		gc.strokeText("6pm", centerPointX - 182, centerPointY + 2);
		gc.strokeText("7", centerPointX - 159, centerPointY - 40);
		gc.strokeText("8", centerPointX - 145, centerPointY - 77);
		gc.strokeText("9", centerPointX - 118, centerPointY - 113);
		gc.strokeText("10", centerPointX - 88, centerPointY - 140);
		gc.strokeText("11", centerPointX - 49, centerPointY - 155);
	}

}