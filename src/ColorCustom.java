import javafx.scene.paint.Color;

public final class ColorCustom {
	public static final Color SLEEP_COLOR = Color.rgb(58, 97, 171);
	public static final Color DRIVING_COLOR = Color.rgb(95, 181, 95);
	public static final Color NOT_DRIVING_COLOR = Color.rgb(218, 89, 84);
	public static final Color OFF_DUTY_COLOR = Color.rgb(253, 252, 236);
}
